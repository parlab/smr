#include "doctest.h"

#include "Frontend/F18Frontend.hpp"
#include "Frontend/Manager.hpp"
#include "Frontend/Source.hpp"

TEST_CASE("Frontend manager tests") {
  SUBCASE("Fetch fortran frontend") {
    frontend::Manager manager;
    CHECK(manager.getFrontend("f90") == manager.getFrontend("f70"));
    CHECK(manager.getFrontend("f90") == manager.getFrontend("f"));
  }
}
