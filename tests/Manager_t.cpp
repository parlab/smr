#include "doctest.h"

#include <fstream>
#include <iostream>

#include "Frontend/Manager.hpp"
#include "Logger/Messages.hpp"

TEST_CASE("Frontend manager tests") {
  SUBCASE("Fetch fortran frontend") {
    frontend::Manager manager;
    CHECK(manager.getFrontend("f90") == manager.getFrontend("f70"));
    CHECK(manager.getFrontend("f90") == manager.getFrontend("f"));
  }
}

TEST_CASE("Frontend Manager Error Tests") {
  SUBCASE("Error on compiling unsupported language") {
    frontend::Manager manager;
    std::string lang = "unsupported", code = "empty";
    CHECK(manager.compile(lang, code) == Msg::FRONT_USUPP_LANG);
  }

  SUBCASE("Error on preprocessing unsupported frontend") {
    frontend::Manager manager;
    mlir::OwningOpRef<mlir::ModuleOp> module;
    std::string lang = "unsupported", code = "empty";
    CHECK(manager.preprocessInput(lang, module.get()) == Msg::FRONT_USUPP_LANG);
    CHECK(manager.preprocessPattern(lang, module.get()) == Msg::FRONT_USUPP_LANG);
    CHECK(manager.preprocessReplacement(lang, module.get()) == Msg::FRONT_USUPP_LANG);
  }
}
