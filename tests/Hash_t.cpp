#include "doctest.h"

#include "Automaton/Hash.hpp"
#include "llvm/Support/raw_ostream.h"
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <fstream>

TEST_CASE("Control Dependency Graph tests") {
  SUBCASE("Should hash string to integers") {
    Hash hasher;
    std::string a("s0"), b("s1"), c("s1");
    CHECK(hasher.hash(std::move(a)) == 0);
    CHECK(hasher.hash(std::move(b)) == 1);
    CHECK(hasher.hash(std::move(c)) == 1);
  }

  SUBCASE("Should hash list of strings to list of integers") {
    Hash hasher;
    std::vector<std::string> path({"s0", "s1", "s1", "s0"});
    std::vector<int> hashed_path, expected_path = {0, 1, 1, 0};
    hasher.hash(path, hashed_path);
    CHECK(hashed_path == expected_path);
  }

  SUBCASE("Should unhash integers to strings") {
    Hash hasher;
    std::string s0("s0"), s1("s1");
    CHECK(hasher.unhash(hasher.hash(std::move(s0))) == "s0");
    CHECK(hasher.unhash(hasher.hash(std::move(s1))) == "s1");
  }

  SUBCASE("Should hash integers to negative integers") {
    Hash hasher;
    CHECK(hasher.hash(0) == -1);
    CHECK(hasher.hash(1) == -2);
  }

  SUBCASE("Should unhash negative integers to indexes") {
    Hash hasher;
    CHECK(hasher.unhashWc(hasher.hash(0)) == 0);
    CHECK(hasher.unhashWc(hasher.hash(1)) == 1);
  }

  SUBCASE("Should unhash negative integers to wildcard strings") {
    Hash hasher;
    CHECK(hasher.unhash(hasher.hash(0)) == "IN_0");
    CHECK(hasher.unhash(hasher.hash(1)) == "IN_1");
  }

  SUBCASE("Should hash list of strings to list of integers") {
    Hash hasher, newHasher;
    std::vector<std::string> path({"s0", "s1", "s1", "s0"});
    std::vector<int> hashed_path, expected_path = {0, 1, 1, 0};
    hasher.hash(path, hashed_path);
    CHECK(hashed_path == expected_path);

    // serialize
    std::ofstream file("/tmp/test.txt");
    boost::archive::text_oarchive oa(file);
    oa << hasher;
    file.close();

    // unserialize
    std::ifstream file2("/tmp/test.txt");
    boost::archive::text_iarchive ia(file2);
    ia >> newHasher;
    file2.close();

    CHECK(newHasher.unhash(expected_path) == hasher.unhash(expected_path));
  }
}
