#include "doctest.h"

#include "Automaton/State.hpp"

TEST_CASE("Atomaton's State tests") {
  SUBCASE("Should create state with ID") {
    State state0(0), state1(1);
    CHECK(state0.getId() == 0);
    CHECK(state1.getId() == 1);
  }

  SUBCASE("Should get pointer to next state on a valid transition") {
    State state0(0);
    state0.addTransition(1, 1);
    CHECK(state0.next(1)->getId() == 1);
  }

  SUBCASE("Should get nullptr on transition to inexistent state") {
    State state0(0);
    CHECK(state0.next(0) == nullptr);
  }

  SUBCASE("Should count the amount of transitions in a state") {
    State state0(0);
    state0.addTransition(1, 1);
    CHECK(state0.getTransitionsCount() == 1);
  }

  SUBCASE("Should iterate through the state's transitions") {
    State state0(0);
    state0.addTransition(1, 1);
    for (auto &transition : state0) {
      CHECK(transition.first == 1);
      CHECK(transition.second->getId() == 1);
    }
  }
}
