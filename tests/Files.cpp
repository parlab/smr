#include "doctest.h"
#include <fstream>
#include <iostream>

#include "Data.hpp"
#include "Files.hpp"
#include "config.h"
#include "llvm/Support/CommandLine.h"

TEST_CASE("Command line validation tests") {
  SUBCASE("Load generic MLIR file as input code") {
    Data data;
    std::string result, expected;
    std::stringstream fileStream(expected);
    llvm::raw_string_ostream resultStream(result), expectedStream(expected);
    auto filepath = std::string(FILES_DIR) + "/mlir/hello-poly.mlir";
    std::ifstream file(filepath);
    int err_code;

    data.getContext()->getOrLoadDialect<mlir::func::FuncDialect>();
    data.getContext()->allowUnregisteredDialects();

    // load expected result
    fileStream << file.rdbuf();
    auto expectedModule = mlir::parseSourceString<mlir::ModuleOp>(
        fileStream.str(), data.getContext());
    expectedModule->print(expectedStream);

    // load function result
    err_code = loadMlirFile(filepath, data);
    data.getInput(0)->print(resultStream);

    if (err_code != 0)
      FAIL("Failed to parse file");
    CHECK(resultStream.str() == expectedStream.str());
  }

  SUBCASE("Load PAT file pure MLIR rewrites") {
    Data data;
    std::string result, expected;
    std::stringstream fileStream(expected);
    llvm::raw_string_ostream pattern_stream(result), replace_stream(expected);
    auto filepath = std::string(FILES_DIR) + "/pat/simple-c-compiled.pat";
    std::ifstream file(filepath);
    int err_code = 0;

    data.getContext()->getOrLoadDialect<mlir::func::FuncDialect>();
    data.getContext()->allowUnregisteredDialects();

    // load function result
    err_code = loadPatFile(filepath, data);

    // file sucessfully loaded?
    if (err_code != 0)
      FAIL("Failed to parse file");

    // loaded expected result?
    data.getPattern(0).print(pattern_stream);
    data.getReplacement(0).print(replace_stream);
    CHECK(pattern_stream.str() == replace_stream.str());
  }

  SUBCASE("Fail on invalid input code") {
    Data data;
    std::string result, expected;
    std::stringstream fileStream(expected);
    llvm::raw_string_ostream resultStream(result), expectedStream(expected);
    auto filepath = std::string(FILES_DIR) + "/mlir/hello-poly.mlir";
    std::ifstream file(filepath);
    int err_code;

    data.getContext()->getOrLoadDialect<mlir::func::FuncDialect>();
    data.getContext()->allowUnregisteredDialects();

    // load expected result
    fileStream << file.rdbuf();
    auto expectedModule = mlir::parseSourceString<mlir::ModuleOp>(
        fileStream.str(), data.getContext());
    expectedModule->print(expectedStream);

    // load function result
    err_code = loadMlirFile(filepath, data);
    data.getInput(0)->print(resultStream);

    if (err_code != 0)
      FAIL("Failed to parse file");
    CHECK(resultStream.str() == expectedStream.str());
  }
}
