import lit.formats

config.name = 'SMR Integration Tests'
config.test_format = lit.formats.ShTest(True)

config.suffixes = ['.lit']

config.test_source_root = os.path.dirname(__file__)
config.test_exec_root = os.path.join(config.smr_obj_root, 'tests')

config.environment['LD_LIBRARY_PATH'] = f'{config.mlir_src_root}'

smr_command = os.path.join(config.smr_obj_root, 'smr/smr')

f90_files = os.path.join(config.smr_src_root, 'tests/files/fortran')

config.substitutions.append(('%vg', 'valgrind --leak-check=full --error-exitcode=1'))
config.substitutions.append(('%smr', smr_command))
config.substitutions.append(('%f90', f90_files))
