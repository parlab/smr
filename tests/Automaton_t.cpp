#include "doctest.h"

#include "Automaton/Automaton.hpp"
#include "Automaton/State.hpp"
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <fstream>

TEST_CASE("Test Atomaton class") {
  SUBCASE("Should create automaton with initial state") {
    Automaton automaton;
    CHECK(automaton.getInitialState()->getId() == 0);
  }

  SUBCASE("Should add new states and return their IDs") {
    Automaton automaton;
    std::vector<int> test1 = {9}, test2 = {8};
    CHECK(automaton.add(test1) == 1);
    CHECK(automaton.add(test2) == 2);
  }

  SUBCASE("Should reach final state on a valid traversal") {
    Automaton automaton;
    std::vector<int> transitions = {1, 2, 3};
    automaton.add(transitions);
    CHECK(automaton.findFirst(transitions)->getIsFinal() == true);
  }

  SUBCASE("Should return nullptr on invalid traversal") {
    Automaton automaton;
    std::vector<int> transitions = {1, 2};
    std::vector<int> test1 = {0}, test2 = {1, 0}, test3 = {1, 2, 0};
    automaton.add(transitions);
    CHECK(automaton.findFirst(test1) == nullptr);
    CHECK(automaton.findFirst(test2) == nullptr);
  }

  SUBCASE("Should reuse existing states and transitions on subpath addition") {
    Automaton automaton;
    std::vector<int> path = {1, 2}, sub_path = {1};
    automaton.add(path);
    automaton.add(sub_path);
    CHECK(automaton.getStatesCount() == 3);
    CHECK(automaton.countTransitions() == 2);
  }

  SUBCASE("Should convert existing state to final on subpath addition") {
    Automaton automaton;
    std::vector<int> path = {1, 2}, sub_path = {1};
    automaton.add(path);
    automaton.add(sub_path);
    CHECK(automaton.findFirst(sub_path)->getIsFinal() == true);
  }

  SUBCASE("Should list all valid paths") {
    Automaton automaton;
    std::string expected, result;
    smr::JagArr<int> paths{{1, 2, 3}, {1, 2, 4}, {1, 5, 4}, {1, 5}, {}};

    // build expected string
    for (auto &path : paths) {
      automaton.add(path);
      for (auto &transition : path)
        expected += std::to_string(transition) + ",";
      expected += "\n";
    }

    // build result string
    for (auto &path : automaton.listPaths()) {
      for (auto &transition : path)
        result += std::to_string(transition) + ",";
      result += "\n";
    }

    CHECK(result == expected);
  }

  SUBCASE("Should return first final state on FIRST traversal type") {
    Automaton automaton;
    std::vector<int> path = {1, 2, 3}, subpath = {1, 2};
    std::vector<int> test = {1, 2, 5, 6};
    automaton.add(path);
    automaton.add(subpath);
    CHECK(automaton.findFirst(test)->getIsFinal() == true);
  }

  SUBCASE("Should return last state if no final state in FIRST traversal") {
    Automaton automaton;
    std::vector<int> path = {1, 2, 3}, subpath = {1, 2};
    std::vector<int> test = {1};
    automaton.add(path);
    automaton.add(subpath);
    CHECK(automaton.findFirst(test)->getId() == 1);
  }

  SUBCASE("Should return nullptr on invalid FIRST traversal type") {
    Automaton automaton;
    std::vector<int> path = {1, 2, 3}, subpath = {1, 2};
    std::vector<int> test = {1, 5, 6};
    automaton.add(path);
    automaton.add(subpath);
    CHECK(automaton.findFirst(test) == nullptr);
  }

  SUBCASE("Should correctly count final states") {
    Automaton automaton;
    std::vector<int> path1 = {1, 2, 3}, subpath1 = {1, 2};
    std::vector<int> path2 = {1, 2, 3}, subpath2 = {1, 2};
    automaton.add(path1);
    automaton.add(subpath1);
    automaton.add(path2);
    automaton.add(subpath2);
    CHECK(automaton.getFinalStatesAmount() == 2);
  }

  SUBCASE("Should return all final states reached") {
    Automaton automaton;
    std::vector<int> path1 = {1, 2, 3}, subpath1 = {1, 2};
    automaton.add(path1);
    automaton.add(subpath1);
    auto result = automaton.findAll(path1);
    CHECK(result.size() == 2);
    CHECK(result[0] != result[1]);
  }

  SUBCASE("Should serialize and unserialize correclty.") {
    Automaton automaton, newAutomaton;
    std::string expected, result;
    smr::JagArr<int> paths{{1, 2, 3}, {1, 2, 4}, {1, 5, 4}, {1, 5}, {}};

    // build expected string
    for (auto &path : paths)
      automaton.add(path);

    // serialize
    std::ofstream file("/tmp/test.txt");
    boost::archive::text_oarchive oa(file);
    oa << automaton;
    file.close();

    // unserialize
    std::ifstream file2("/tmp/test.txt");
    boost::archive::text_iarchive ia(file2);
    ia >> newAutomaton;
    file2.close();

    CHECK(automaton.listPaths() == newAutomaton.listPaths());
  }
}
