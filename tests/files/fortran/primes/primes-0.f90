subroutine primes(candidate, residue, j, isprime)
  integer                  :: candidate
  integer                  :: residue
  integer                  :: j
  logical                  :: isprime
  do j = 2,int(sqrt(real(candidate)))
    residue = mod(candidate,j)
    if ( residue == 0 ) then
      isprime = .false.
      exit
    endif
  enddo
end subroutine primes