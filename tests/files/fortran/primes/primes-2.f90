subroutine primes(prime, number_primes, candidate, residue, j, isprime)
  integer, dimension(1000) :: prime
  integer                  :: number_primes
  integer                  :: candidate
  integer                  :: residue
  integer                  :: j
  logical                  :: isprime
  do while ( number_primes < size(prime) )
    isprime = .true.
    do j = 2,int(sqrt(real(candidate)))
      residue = mod(candidate,j)
      if ( residue == 0 ) then
        isprime = .false.
        exit
      endif
    enddo
    if ( isprime ) then
      number_primes = number_primes + 1
      prime(number_primes) = candidate
    endif

    candidate = candidate + 1
  enddo
end subroutine primes