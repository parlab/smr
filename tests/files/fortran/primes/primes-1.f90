subroutine primes(prime, number_primes, candidate, isprime)
  integer, dimension(1000) :: prime
  integer                  :: number_primes
  integer                  :: candidate
  logical                  :: isprime
  if ( isprime ) then
    number_primes = number_primes + 1
    prime(number_primes) = candidate
  endif
end subroutine primes