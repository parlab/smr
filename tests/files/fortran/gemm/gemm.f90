subroutine abs_sum(abs_, n_, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc)
    real :: abs_
    integer :: n_, v_, t_ ! v=val & t=test 
    integer :: m, n, k
    integer :: lda, ldb, ldc
    real, dimension(m * n) :: C
    real, dimension(m * k) :: A
    real, dimension(k * n) :: B
    real, dimension(m, n) :: C2
    real, dimension(m, k) :: A2
    real, dimension(k, n) :: B2
    real :: alpha, beta
    real :: aa, bb, cc

    !!! GEMM 10 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0
            do i = 1, k
                aa = A(mm + i * lda)
                bb = B(nn + i * ldb)
                cc = cc + alpha * aa * bb
            end do
            C(mm + nn * ldc) = cc + C(mm + nn * ldc)
        end do
    end do
    !!!!!!!!!!!!!!

    !!! GEMM 10 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0
            do i = 1, k
                aa = A(mm + i * lda)
                bb = B(nn + i * ldb)
                cc = cc + alpha * aa * bb
            end do
            C(mm + nn * ldc) = cc + C(mm + nn * ldc)
        end do
    end do
    !!!!!!!!!!!!!!

    !!! GEMM 7 !!!
    do mm = 1, m
        do nn = 1, n
          cc = 0
          do i = 1, k
            aa = A2(mm, i)
            bb = B2(nn, i)
            cc = cc + aa * bb
          end do
          C2(mm, nn) = beta * C2(mm, nn) + cc
        end do
    end do
    !!!!!!!!!!!!!!

    !!! GEMM 3 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0.0
            do i = 1, k
                aa = A2(mm, i)
                bb = B2(nn, i)
                cc = cc + alpha * aa * bb;
            end do
            C2(mm, nn) = cc + C2(mm, nn) * beta
        end do
    end do
    !!!!!!!!!!!!!!

    !!! GEMM 5 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0.0
            do i = 1, k
                aa = A2(mm, i)
                bb = B2(nn, i)
                cc = cc + aa * bb
            end do
            C2(mm, nn) = alpha * cc
        end do
    end do
    !!!!!!!!!!!!!!

    IF (n_==0) THEN
        v_ = 1
        t_ = 0
    ELSE
        v_ = 0
        t_ = 1
    END IF
    
    !!! GEMM 2 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0.0
            do i = 1, k
                aa = A2(mm, i)
                bb = B2(nn, i)
                cc = cc + aa * bb;
            end do
            C2(mm, nn) = C2(mm, nn) * beta + alpha * cc
        end do
    end do
    !!!!!!!!!!!!!

    !!! GEMM 10 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0
            do i = 1, k
                aa = A(mm + i * lda)
                bb = B(nn + i * ldb)
                cc = cc + alpha * aa * bb
            end do
            C(mm + nn * ldc) = cc + C(mm + nn * ldc)
        end do
    end do
    !!!!!!!!!!!!!!

    !!! GEMM 8 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0
            do i = 1, k
                aa = A(mm + i * lda)
                bb = B(nn + i * ldb)
                cc = cc + aa * bb
            end do
            C(mm + nn * ldc) = beta * C(mm + nn * ldc) + cc
        end do
    end do
    !!!!!!!!!!!!!

    !!! GEMM 5 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0.0
            do i = 1, k
                bb = B2(nn, i)
                aa = A2(mm, i)
                cc = aa * bb + cc
            end do
            C2(mm, nn) = cc * alpha
        end do
    end do
    !!!!!!!!!!!!!!

    IF (abs_ > 0) THEN
        IF (t_ == 1) THEN
            v_ = 1
        ELSE
            v_ = v_ - 1
            IF (v_ == 1) THEN
                t_ = 0
            END IF
        END IF
    END IF

    !!! GEMM 3 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0.0
            do i = 1, k
                aa = A2(mm, i)
                bb = B2(nn, i)
                cc = cc + alpha * aa * bb;
            end do
            C2(mm, nn) = cc + C2(mm, nn) * beta
        end do
    end do
    !!!!!!!!!!!!!!

    abs_ = 0
    DO i = 1, n_
        IF (aray(i) > 0) THEN
            abs_ = abs_ + abs_
        ELSE IF (aray(i) < 0) THEN
            abs_ = abs_ - abs_
        END IF
    END DO
    
    !!! GEMM 3 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0.0
            do i = 1, k
                aa = A2(mm, i)
                bb = B2(nn, i)
                cc = cc + alpha * aa * bb;
            end do
            C2(mm, nn) = cc + C2(mm, nn) * beta
        end do
    end do

    !!! GEMM 5 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0.0
            do i = 1, k
                aa = A2(mm, i)
                bb = B2(nn, i)
                cc = aa * bb + cc
            end do
            C2(mm, nn) = cc * alpha
        end do
    end do
    !!!!!!!!!!!!!!

    !!! GEMM 1 !!!
    do mm = 1, m
    do nn = 1, n
            cc = 0.0
            do i = 1, k
                bb = B(nn + i * ldb)
                aa = A(mm + i * lda)
                cc = cc + alpha * aa * bb;
            end do
            C(mm + nn * ldc) = cc + beta * C(mm + nn * ldc)
        end do
    end do
    !!!!!!!!!!!!!!
    
    !!! GEEM 4 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0.0
            do i = 1, k
                aa = A2(mm, i)
                bb = B2(nn, i)
                cc = cc + alpha * aa * bb
            end do
            C2(mm, nn) = cc
        end do
    end do
    !!!!!!!!!!!!!!

    !!! GEMM 6 !!!
    do mm = 1, m
        do nn = 1, n
          do i = 1, k
            bb = B2(nn, i)
            aa = A2(mm, i)
            C2(mm, nn) = alpha * bb * aa + C2(mm, nn)
          end do
        end do
    end do
    !!!!!!!!!!!!!!

    !!! GEMM 2 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0.0
            do i = 1, k
                aa = A2(mm, i)
                bb = B2(nn, i)
                cc = cc + aa * bb;
            end do
            C2(mm, nn) = C2(mm, nn) * beta + alpha * cc
        end do
    end do
    !!!!!!!!!!!!!

    !!! GEEM 4 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0.0
            do i = 1, k
                bb = B2(nn, i)
                aa = A2(mm, i)
                cc = cc + alpha * aa * bb
            end do
            C2(mm, nn) = cc
        end do
    end do
    !!!!!!!!!!!!!!

    !!! GEEM 4 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0.0
            do i = 1, k
                bb = B2(nn, i)
                aa = A2(mm, i)
                cc = cc + alpha * aa * bb
            end do
            C2(mm, nn) = cc
        end do
    end do
    !!!!!!!!!!!!!!

    !!! GEEM 4 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0.0
            do i = 1, k
                aa = A2(mm, i)
                bb = B2(nn, i)
                cc = cc + alpha * aa * bb
            end do
            C2(mm, nn) = cc
        end do
    end do
    !!!!!!!!!!!!!!

    !!! GEMM 5 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0.0
            do i = 1, k
                aa = A2(mm, i)
                bb = B2(nn, i)
                cc = cc + aa * bb
            end do
            C2(mm, nn) = alpha * cc
        end do
    end do
    !!!!!!!!!!!!!!

    !!! GEMM 6 !!!
    do mm = 1, m
        do nn = 1, n
          do i = 1, k
            aa = A2(mm, i)
            bb = B2(nn, i)
            C2(mm, nn) = C2(mm, nn) + alpha * bb * aa
          end do
        end do
    end do
    !!!!!!!!!!!!!!

    !!! GEMM 5 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0.0
            do i = 1, k
                bb = B2(nn, i)
                aa = A2(mm, i)
                cc = cc + aa * bb
            end do
            C2(mm, nn) = alpha * cc
        end do
    end do
    !!!!!!!!!!!!!!

    !!! GEMM 6 !!!
    do mm = 1, m
        do nn = 1, n
          do i = 1, k
            aa = A2(mm, i)
            bb = B2(nn, i)
            C2(mm, nn) = C2(mm, nn) + alpha * aa * bb
          end do
        end do
    end do
    !!!!!!!!!!!!!!

    PRINT *, 'A?'
    READ *, NA
    IF (NA.LE.0) THEN
      PRINT *, 'A must be a positive integer.'
      STOP
    END IF
    PRINT *, 'B?'
    READ *, NB
    IF (NB.LE.0) THEN
      PRINT *, 'B must be a positive integer.'
      STOP
    END IF
    PRINT *, 'The GCD of', NA, ' and', NB, ' is', NGCD(NA, NB), '.'
    STOP

    !!! GEMM 6 !!!
    do mm = 1, m
        do nn = 1, n
          do i = 1, k
            bb = B2(nn, i)
            aa = A2(mm, i)
            C2(mm, nn) = alpha * bb * aa + C2(mm, nn)
          end do
        end do
    end do
    !!!!!!!!!!!!!!

    !!! GEMM 9 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0
            do i = 1, k
                aa = A(mm + i * lda)
                bb = B(nn + i * ldb)
                cc = cc + aa * bb
            end do
            C(mm + nn * ldc) = alpha * cc + beta * C(mm + nn * ldc)
        end do
    end do
    !!!!!!!!!!!!!!

    !!! GEMM 9 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0
            do i = 1, k
                bb = B(i * ldb + nn)
                aa = A(i * lda + mm)
                cc = cc + aa * bb
            end do
            C(mm + nn * ldc) = beta * C(mm + nn * ldc) + alpha * cc
        end do
    end do
    !!!!!!!!!!!!!!

    !!! GEMM 6 !!!
    do mm = 1, m
        do nn = 1, n
          do i = 1, k
            bb = B2(nn, i)
            aa = A2(mm, i)
            C2(mm, nn) = alpha * bb * aa + C2(mm, nn)
          end do
        end do
    end do
    !!!!!!!!!!!!!!

    do i=1,5
        do j=1,5
           A2(i,j)= 100*i+j
        end do
    end do

    !!! GEMM 6 !!!
    do mm = 1, m
        do nn = 1, n
          do i = 1, k
            bb = B2(nn, i)
            aa = A2(mm, i)
            C2(mm, nn) = alpha * bb * aa + C2(mm, nn)
          end do
        end do
    end do
    !!!!!!!!!!!!!!

    !!! GEMM 8 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0
            do i = 1, k
                bb = B(nn + i * ldb)
                aa = A(mm + i * lda)
                cc = aa * bb + cc
            end do
            C(nn * ldc + mm) = cc + C(nn * ldc + mm) * beta
        end do
    end do
    !!!!!!!!!!!!!


    !!! GEMM 9 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0
            do i = 1, k
                bb = B(i * ldb + nn)
                aa = A(i * lda + mm)
                cc = cc + aa * bb
            end do
            C(mm + nn * ldc) = beta * C(mm + nn * ldc) + alpha * cc
        end do
    end do
    !!!!!!!!!!!!!!

    !!! GEMM 10 !!!
    do mm = 1, m
        do nn = 1, n
            cc = 0
            do i = 1, k
                aa = A(mm + i * lda)
                bb = B(nn + i * ldb)
                cc = cc + alpha * aa * bb
            end do
            C(mm + nn * ldc) = cc + C(mm + nn * ldc)
        end do
    end do
    !!!!!!!!!!!!!!
end subroutine
