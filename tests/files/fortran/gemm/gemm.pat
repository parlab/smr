f90 {
subroutine gemm0(m, n, k, alpha, A, lda, B, ldb, beta, C, ldc)
  integer :: m, n, k
  integer :: lda, ldb, ldc
  real, dimension(m * n) :: C
  real, dimension(m * k) :: A
  real, dimension(k * n) :: B
  real :: alpha, beta
  real :: aa, bb, cc

  IF (alpha > beta) THEN
    alpha = 0;
  ELSE
    beta = 0;
  END IF

  !do mm = 1, m
  !  do nn = 1, n
  !    cc = 0.0
  !    do i = 1, k
  !      aa = A(mm + i * lda)
  !      bb = B(nn + i * ldb)
  !      cc = cc + aa * bb
  !    end do
  !    C(mm + nn * ldc) = C(mm + nn * ldc) * beta + alpha * cc
  !  end do
  !end do
end subroutine
}={
subroutine gemm0(m, n, k, alpha, A, lda, B, ldb, beta, C, ldc)
  integer :: m, n, k
  integer :: lda, ldb, ldc
  real, dimension(m * n) :: C
  real, dimension(m * k) :: A
  real, dimension(k * n) :: B
  real :: alpha, beta
  real :: aa, bb, cc

  print*,"Pattern gemm0 replaced"
end subroutine
}

f90 {
subroutine gemm1(m, n, k, alpha, A, lda, B, ldb, beta, C, ldc)
  integer :: m, n, k
  integer :: lda, ldb, ldc
  real, dimension(m * n) :: C
  real, dimension(m * k) :: A
  real, dimension(k * n) :: B
  real :: alpha, beta
  real :: aa, bb, cc

  do mm = 1, m
    do nn = 1, n
      cc = 0.0
      do i = 1, k
        aa = A(mm + i * lda)
        bb = B(nn + i * ldb)
        cc = cc + alpha * aa * bb;
      end do
      C(mm + nn * ldc) = cc + beta * C(mm + nn * ldc)
    end do
  end do
end subroutine
}={
subroutine gemm1(m, n, k, alpha, A, lda, B, ldb, beta, C, ldc)
  integer :: m, n, k
  integer :: lda, ldb, ldc
  real, dimension(m * n) :: C
  real, dimension(m * k) :: A
  real, dimension(k * n) :: B
  real :: alpha, beta
  real :: aa, bb, cc

  print*,"Pattern gemm1 replaced"
end subroutine
}

f90 {
subroutine gemm2(m, n, k, alpha, A, B, beta, C)
  integer :: m, n, k
  real, dimension(m, n) :: C
  real, dimension(m, k) :: A
  real, dimension(k, n) :: B
  real :: alpha, beta
  real :: aa, bb, cc

  do mm = 1, m
    do nn = 1, n
      cc = 0.0
      do i = 1, k
        aa = A(mm, i)
        bb = B(nn, i)
        cc = cc + aa * bb;
      end do
      C(mm, nn) = C(mm, nn) * beta + alpha * cc
    end do
  end do
end subroutine
}={
subroutine gemm2(m, n, k, alpha, A, B, beta, C)
  integer :: m, n, k
  real, dimension(m, n) :: C
  real, dimension(m, k) :: A
  real, dimension(k, n) :: B
  real :: alpha, beta
  real :: aa, bb, cc

  print*,"Pattern gemm2 replaced"
end subroutine
}

f90 {
subroutine gemm3(m, n, k, alpha, A, B, beta, C)
  integer :: m, n, k
  real, dimension(m, n) :: C
  real, dimension(m, k) :: A
  real, dimension(k, n) :: B
  real :: alpha, beta
  real :: aa, bb, cc

  do mm = 1, m
    do nn = 1, n
      cc = 0.0
      do i = 1, k
        aa = A(mm, i)
        bb = B(nn, i)
        cc = cc + alpha * aa * bb;
      end do
      C(mm, nn) = cc + C(mm, nn) * beta
    end do
  end do
end subroutine
}={
subroutine gemm3(m, n, k, alpha, A, B, beta, C)
  integer :: m, n, k
  real, dimension(m, n) :: C
  real, dimension(m, k) :: A
  real, dimension(k, n) :: B
  real :: alpha, beta
  real :: aa, bb, cc

  print*,"Pattern gemm3 replaced"
end subroutine
}

f90 {
subroutine gemm4(m, n, k, alpha, A, B, C)
  integer :: m, n, k
  real, dimension(m, n) :: C
  real, dimension(m, k) :: A
  real, dimension(k, n) :: B
  real :: alpha
  real :: aa, bb, cc

  do mm = 1, m
    do nn = 1, n
      cc = 0.0
      do i = 1, k
        aa = A(mm, i)
        bb = B(nn, i)
        cc = cc + alpha * aa * bb
      end do
      C(mm, nn) = cc
    end do
  end do
end subroutine
}={
subroutine gemm4(m, n, k, alpha, A, B, C)
  integer :: m, n, k
  real, dimension(m, n) :: C
  real, dimension(m, k) :: A
  real, dimension(k, n) :: B
  real :: alpha
  real :: aa, bb, cc

  print*,"Pattern gemm4 replaced"
end subroutine
}

f90 {
subroutine gemm5(m, n, k, alpha, A, B, C)
  integer :: m, n, k
  real, dimension(m, n) :: C
  real, dimension(m, k) :: A
  real, dimension(k, n) :: B
  real :: alpha
  real :: aa, bb, cc

  do mm = 1, m
    do nn = 1, n
      cc = 0.0
      do i = 1, k
        aa = A(mm, i)
        bb = B(nn, i)
        cc = cc + aa * bb
      end do
      C(mm, nn) = alpha * cc
    end do
  end do
end subroutine
}={
subroutine gemm5(m, n, k, alpha, A, B, C)
  integer :: m, n, k
  real, dimension(m, n) :: C
  real, dimension(m, k) :: A
  real, dimension(k, n) :: B
  real :: alpha
  real :: aa, bb, cc

  print*,"Pattern gemm5 replaced"
end subroutine
}

f90 {
subroutine gemm6(m, n, k, alpha, A, B, C)
  integer :: m, n, k
  real, dimension(m, n) :: C
  real, dimension(m, k) :: A
  real, dimension(k, n) :: B
  real :: alpha
  real :: aa, bb

  do mm = 1, m
    do nn = 1, n
      do i = 1, k
        aa = A(mm, i)
        bb = B(nn, i)
        C(mm, nn) = C(mm, nn) + alpha * aa * bb
      end do
    end do
  end do
end subroutine
}={
subroutine gemm6(m, n, k, alpha, A, B, C)
  integer :: m, n, k
  real, dimension(m, n) :: C
  real, dimension(m, k) :: A
  real, dimension(k, n) :: B
  real :: alpha
  real :: aa, bb

  print*,"Pattern gemm6 replaced"
end subroutine
}

f90 {
subroutine gemm7(m, n, k, A, B, beta, C)
  integer :: m, n, k
  real, dimension(m, n) :: C
  real, dimension(m, k) :: A
  real, dimension(k, n) :: B
  real :: beta
  real :: aa, bb, cc

  do mm = 1, m
    do nn = 1, n
      cc = 0
      do i = 1, k
        aa = A(mm, i)
        bb = B(nn, i)
        cc = cc + aa * bb
      end do
      C(mm, nn) = beta * C(mm, nn) + cc
    end do
  end do
end subroutine
}={
subroutine gemm7(m, n, k, A, B, beta, C)
  integer :: m, n, k
  real, dimension(m, n) :: C
  real, dimension(m, k) :: A
  real, dimension(k, n) :: B
  real :: beta
  real :: aa, bb, cc

  print*,"Pattern gemm7 replaced"
end subroutine
}

f90 {
subroutine gemm8(m, n, k, A, lda, B, ldb, beta, C, ldc)
  integer :: m, n, k
  integer :: lda, ldb, ldc
  real, dimension(m * n) :: C
  real, dimension(m * k) :: A
  real, dimension(k * n) :: B
  real :: beta
  real :: aa, bb, cc

  do mm = 1, m
    do nn = 1, n
      cc = 0
      do i = 1, k
        aa = A(mm + i * lda)
        bb = B(nn + i * ldb)
        cc = cc + aa * bb
      end do
      C(mm + nn * ldc) = beta * C(mm + nn * ldc) + cc
    end do
  end do
end subroutine

}={
subroutine gemm8(m, n, k, A, lda, B, ldb, beta, C, ldc)
  integer :: m, n, k
  integer :: lda, ldb, ldc
  real, dimension(m * n) :: C
  real, dimension(m * k) :: A
  real, dimension(k * n) :: B
  real :: beta
  real :: aa, bb, cc

  print*,"Pattern gemm8 replaced"
end subroutine
}

f90 {
subroutine gemm9(m, n, k, alpha, A, lda, B, ldb, beta, C, ldc)
  integer :: m, n, k
  integer :: lda, ldb, ldc
  real, dimension(m * n) :: C
  real, dimension(m * k) :: A
  real, dimension(k * n) :: B
  real :: alpha, beta
  real :: aa, bb, cc

  do mm = 1, m
    do nn = 1, n
      cc = 0
      do i = 1, k
        aa = A(mm + i * lda)
        bb = B(nn + i * ldb)
        cc = cc + aa * bb
      end do
      C(mm + nn * ldc) = alpha * cc + beta * C(mm + nn * ldc)
    end do
  end do
end subroutine
}={
subroutine gemm9(m, n, k, alpha, A, lda, B, ldb, beta, C, ldc)
  integer :: m, n, k
  integer :: lda, ldb, ldc
  real, dimension(m * n) :: C
  real, dimension(m * k) :: A
  real, dimension(k * n) :: B
  real :: alpha, beta
  real :: aa, bb, cc

  print*,"Pattern gemm9 replaced"
end subroutine
}

f90 {
subroutine gemm10(m, n, k, alpha, A, lda, B, ldb, C, ldc)
  integer :: m, n, k
  integer :: lda, ldb, ldc
  real, dimension(m * n) :: C
  real, dimension(m * k) :: A
  real, dimension(k * n) :: B
  real :: alpha
  real :: aa, bb, cc

  do mm = 1, m
    do nn = 1, n
      cc = 0
      do i = 1, k
        aa = A(mm + i * lda)
        bb = B(nn + i * ldb)
        cc = cc + alpha * aa * bb
      end do
      C(mm + nn * ldc) = cc + C(mm + nn * ldc)
    end do
  end do
end subroutine
}={
subroutine gemm10(m, n, k, alpha, A, lda, B, ldb, C, ldc)
  integer :: m, n, k
  integer :: lda, ldb, ldc
  real, dimension(m * n) :: C
  real, dimension(m * k) :: A
  real, dimension(k * n) :: B
  real :: alpha
  real :: aa, bb, cc

  print*,"Pattern gemm10 replaced"
end subroutine
}

f90 {
subroutine gemm11(m, n, k, alpha, A, lda, B, ldb, C, ldc)
  integer :: m, n, k
  integer :: lda, ldb, ldc
  real, dimension(m * n) :: C
  real, dimension(m * k) :: A
  real, dimension(k * n) :: B
  real :: alpha
  real :: aa, bb

  do mm = 1, m
    do nn = 1, n
      do i = 1, k
        aa = A(mm + i * lda)
        bb = B(nn + i * ldb)
        C(mm + nn * ldc) = C(mm + nn * ldc) + alpha * aa * bb
      end do
    end do
  end do
end subroutine
}={
subroutine gemm11(m, n, k, alpha, A, lda, B, ldb, C, ldc)
  integer :: m, n, k
  integer :: lda, ldb, ldc
  real, dimension(m * n) :: C
  real, dimension(m * k) :: A
  real, dimension(k * n) :: B
  real :: alpha
  real :: aa, bb

  print*,"Pattern gemm11 replaced"
end subroutine
}

f90 {
subroutine gemm12(m, n, k, A, lda, B, ldb, C, ldc)
  integer :: m, n, k
  integer :: lda, ldb, ldc
  real, dimension(m * n) :: C
  real, dimension(m * k) :: A
  real, dimension(k * n) :: B
  real :: aa, bb, cc

  do mm = 1, m
    do nn = 1, n
      cc = 0
      do i = 1, k
        aa = A(mm + i * lda)
        bb = B(nn + i * ldb)
        cc = cc + aa * bb
      end do
      C(mm + nn * ldc) = cc
    end do
  end do
end subroutine
}={
subroutine gemm12(m, n, k, A, lda, B, ldb, C, ldc)
  integer :: m, n, k
  integer :: lda, ldb, ldc
  real, dimension(m * n) :: C
  real, dimension(m * k) :: A
  real, dimension(k * n) :: B
  real :: aa, bb, cc

  print*,"Pattern gemm12 replaced"
end subroutine
}

f90 {
subroutine gemm13(m, n, k, A, lda, B, ldb, C, ldc)
  integer :: m, n, k
  integer :: lda, ldb, ldc
  real, dimension(m * n) :: C
  real, dimension(m * k) :: A
  real, dimension(k * n) :: B
  real :: aa, bb

  do mm = 1, m
    do nn = 1, n
      C(mm + nn * ldc) = 0
      do i = 1, k
        aa = A(mm + i * lda)
        bb = B(nn + i * ldb)
        C(mm + nn * ldc) = C(mm + nn * ldc) + aa * bb
      end do
    end do
  end do
end subroutine
}={
subroutine gemm13(m, n, k, A, lda, B, ldb, C, ldc)
  integer :: m, n, k
  integer :: lda, ldb, ldc
  real, dimension(m * n) :: C
  real, dimension(m * k) :: A
  real, dimension(k * n) :: B
  real :: aa, bb

  print*,"Pattern gemm13 replaced"
end subroutine
}

f90 {
subroutine gemm14(m, n, k, A, B, C)
  integer :: m, n, k
  real, dimension(m, n) :: C
  real, dimension(m, k) :: A
  real, dimension(k, n) :: B
  real :: aa, bb, cc

  do mm = 1, m
    do nn = 1, n
      cc = 0
      do i = 1, k
        aa = A(mm, i)
        bb = B(nn, i)
        cc = cc + aa * bb
      end do
      C(mm, nn) = cc
    end do
  end do
end subroutine
}={
subroutine gemm14(m, n, k, A, B, C)
  integer :: m, n, k
  real, dimension(m, n) :: C
  real, dimension(m, k) :: A
  real, dimension(k, n) :: B
  real :: aa, bb, cc

  print*,"Pattern gemm14 replaced"
end subroutine
}

f90 {
subroutine gemm15(m, n, k, A, B, C)
  integer :: m, n, k
  real, dimension(m, n) :: C
  real, dimension(m, k) :: A
  real, dimension(k, n) :: B
  real :: aa, bb

  do mm = 1, m
    do nn = 1, n
      C(mm, nn) = 0
      do i = 1, k
        aa = A(mm, i)
        bb = B(nn, i)
        C(mm, nn) = C(mm, nn) + aa * bb
      end do
    end do
  end do
end subroutine
}={
subroutine gemm15(m, n, k, A, B, C)
  integer :: m, n, k
  real, dimension(m, n) :: C
  real, dimension(m, k) :: A
  real, dimension(k, n) :: B
  real :: aa, bb

  print*,"Pattern gemm15 replaced"
end subroutine
}