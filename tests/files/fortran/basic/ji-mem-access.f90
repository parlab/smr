subroutine ji_mx_access(A, n)
    integer :: n, i, j
    double precision, dimension(n, n) :: A

    do i = 1, n
        do j = 1, n
            A(j, i) = n ! j for rows and i for columns
        end do
    end do

end subroutine
