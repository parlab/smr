f90 {
  subroutine sum(a, b, c)
    integer :: a, b, c
    do i = 1, 10
      c = a + b
    end do
  end subroutine
}={
  subroutine sum(a, b, c)
    integer :: a, b, c
    do i = 1, 10
      c = a + b
    end do
  end subroutine
}

f90 {
  subroutine sum(a, c)
    integer :: a, c
    do i = 1, 10
      c = a + a
    end do
  end subroutine
}={
  subroutine sum(a, c)
    integer :: a, c
    do i = 1, 10
      c = a + a
    end do
  end subroutine
}
