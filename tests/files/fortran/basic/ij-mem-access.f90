subroutine ij_mx_access(A, n)
    integer :: n, i, j
    double precision, dimension(n, n) :: A

    do i = 1, n
        do j = 1, n
            A(i, j) = n ! i for rows and j for columns
        end do
    end do

end subroutine
