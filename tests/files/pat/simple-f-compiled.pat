mlir {module  {
  func @_QQmain() {
    %0 = "arith.constant"() {value = 13 : index} : () -> index
    %1 = "arith.constant"() {value = 4 : i32} : () -> i32
    %2 = "arith.constant"() {value = -1 : i32} : () -> i32
    %3 = "arith.constant"() {value = true} : () -> i1
    "fir.if"(%3) ( {
      %4 = "fir.address_of"() {symbol = @_QQcl.2F746D702F66696C6569496973556400} : () -> !fir.ref<!fir.char<1,16>>
      %5 = "fir.convert"(%4) : (!fir.ref<!fir.char<1,16>>) -> !fir.ref<i8>
      %6 = "fir.call"(%2, %5, %1) {callee = @_FortranAioBeginExternalListOutput} : (i32, !fir.ref<i8>, i32) -> !fir.ref<i8>
      %7 = "fir.address_of"() {symbol = @_QQcl.48656C6C6F2C20776F726C6421} : () -> !fir.ref<!fir.char<1,13>>
      %8 = "fir.convert"(%7) : (!fir.ref<!fir.char<1,13>>) -> !fir.ref<i8>
      %9 = "fir.convert"(%0) : (index) -> i64
      %10 = "fir.call"(%6, %8, %9) {callee = @_FortranAioOutputAscii} : (!fir.ref<i8>, !fir.ref<i8>, i64) -> i1
      %11 = "fir.call"(%6) {callee = @_FortranAioEndIoStatement} : (!fir.ref<i8>) -> i32
      "fir.result"() : () -> ()
    },  {
      "fir.result"() : () -> ()
    }) : (i1) -> ()
    "std.return"() : () -> ()
  }
  func private @_FortranAioBeginExternalListOutput(i32, !fir.ref<i8>, i32) -> !fir.ref<i8> attributes {fir.io, fir.runtime}
  "fir.global"() ( {
    %0 = "fir.string_lit"() {size = 16 : i64, value = "/tmp/fileiIisUd\00"} : () -> !fir.char<1,16>
    "fir.has_value"(%0) : (!fir.char<1,16>) -> ()
  }) {constant, linkName = "linkonce", sym_name = "_QQcl.2F746D702F66696C6569496973556400", symref = @_QQcl.2F746D702F66696C6569496973556400, type = !fir.char<1,16>} : () -> ()
  func private @_FortranAioOutputAscii(!fir.ref<i8>, !fir.ref<i8>, i64) -> i1 attributes {fir.io, fir.runtime}
  "fir.global"() ( {
    %0 = "fir.string_lit"() {size = 13 : i64, value = "Hello, world!"} : () -> !fir.char<1,13>
    "fir.has_value"(%0) : (!fir.char<1,13>) -> ()
  }) {constant, linkName = "linkonce", sym_name = "_QQcl.48656C6C6F2C20776F726C6421", symref = @_QQcl.48656C6C6F2C20776F726C6421, type = !fir.char<1,13>} : () -> ()
  func private @_FortranAioEndIoStatement(!fir.ref<i8>) -> i32 attributes {fir.io, fir.runtime}
}


} = {module  {
  func @_QQmain() {
    %0 = "arith.constant"() {value = 13 : index} : () -> index
    %1 = "arith.constant"() {value = 4 : i32} : () -> i32
    %2 = "arith.constant"() {value = -1 : i32} : () -> i32
    %3 = "arith.constant"() {value = true} : () -> i1
    "fir.if"(%3) ( {
      %4 = "fir.address_of"() {symbol = @_QQcl.2F746D702F66696C6569496973556400} : () -> !fir.ref<!fir.char<1,16>>
      %5 = "fir.convert"(%4) : (!fir.ref<!fir.char<1,16>>) -> !fir.ref<i8>
      %6 = "fir.call"(%2, %5, %1) {callee = @_FortranAioBeginExternalListOutput} : (i32, !fir.ref<i8>, i32) -> !fir.ref<i8>
      %7 = "fir.address_of"() {symbol = @_QQcl.48656C6C6F2C20776F726C6421} : () -> !fir.ref<!fir.char<1,13>>
      %8 = "fir.convert"(%7) : (!fir.ref<!fir.char<1,13>>) -> !fir.ref<i8>
      %9 = "fir.convert"(%0) : (index) -> i64
      %10 = "fir.call"(%6, %8, %9) {callee = @_FortranAioOutputAscii} : (!fir.ref<i8>, !fir.ref<i8>, i64) -> i1
      %11 = "fir.call"(%6) {callee = @_FortranAioEndIoStatement} : (!fir.ref<i8>) -> i32
      "fir.result"() : () -> ()
    },  {
      "fir.result"() : () -> ()
    }) : (i1) -> ()
    "std.return"() : () -> ()
  }
  func private @_FortranAioBeginExternalListOutput(i32, !fir.ref<i8>, i32) -> !fir.ref<i8> attributes {fir.io, fir.runtime}
  "fir.global"() ( {
    %0 = "fir.string_lit"() {size = 16 : i64, value = "/tmp/fileiIisUd\00"} : () -> !fir.char<1,16>
    "fir.has_value"(%0) : (!fir.char<1,16>) -> ()
  }) {constant, linkName = "linkonce", sym_name = "_QQcl.2F746D702F66696C6569496973556400", symref = @_QQcl.2F746D702F66696C6569496973556400, type = !fir.char<1,16>} : () -> ()
  func private @_FortranAioOutputAscii(!fir.ref<i8>, !fir.ref<i8>, i64) -> i1 attributes {fir.io, fir.runtime}
  "fir.global"() ( {
    %0 = "fir.string_lit"() {size = 13 : i64, value = "Hello, world!"} : () -> !fir.char<1,13>
    "fir.has_value"(%0) : (!fir.char<1,13>) -> ()
  }) {constant, linkName = "linkonce", sym_name = "_QQcl.48656C6C6F2C20776F726C6421", symref = @_QQcl.48656C6C6F2C20776F726C6421, type = !fir.char<1,13>} : () -> ()
  func private @_FortranAioEndIoStatement(!fir.ref<i8>) -> i32 attributes {fir.io, fir.runtime}
}


}

