#map = affine_map<() -> (0)>
"builtin.module"() ({
  "llvm.func"() ({
  }) {function_type = !llvm.func<i32 (ptr<i8>, ...)>, linkage = #llvm.linkage<external>, sym_name = "printf"} : () -> ()
  "llvm.mlir.global"() ({
  }) {constant, global_type = !llvm.array<13 x i8>, linkage = #llvm.linkage<internal>, sym_name = "str0", unnamed_addr = 0 : i64, value = "Hello World!\00"} : () -> ()
  "memref.global"() {initial_value = dense<1> : tensor<1xi32>, sym_name = "glb", type = memref<1xi32>} : () -> ()
  "func.func"() ({
    %0 = "arith.constant"() {value = 0 : i32} : () -> i32
    %1 = "arith.constant"() {value = 1 : i32} : () -> i32
    %2 = "memref.get_global"() {name = @glb} : () -> memref<1xi32>
    %3 = "affine.load"(%2) {map = #map} : (memref<1xi32>) -> i32
    %4 = "arith.cmpi"(%3, %1) {predicate = 0 : i64} : (i32, i32) -> i1
    "scf.if"(%4) ({
      %5 = "llvm.mlir.addressof"() {global_name = @str0} : () -> !llvm.ptr<array<13 x i8>>
      %6 = "llvm.getelementptr"(%5, %0, %0) {structIndices = dense<-2147483648> : tensor<2xi32>} : (!llvm.ptr<array<13 x i8>>, i32, i32) -> !llvm.ptr<i8>
      %7 = "llvm.call"(%6) {callee = @printf} : (!llvm.ptr<i8>) -> i32
      "scf.yield"() : () -> ()
    }, {
    }) : (i1) -> ()
    "func.return"(%0) : (i32) -> ()
  }) {function_type = () -> i32, llvm.linkage = #llvm.linkage<external>, sym_name = "main"} : () -> ()
}) {dlti.dl_spec = #dlti.dl_spec<#dlti.dl_entry<"dlti.endianness", "little">, #dlti.dl_entry<i64, dense<64> : vector<2xi32>>, #dlti.dl_entry<f80, dense<128> : vector<2xi32>>, #dlti.dl_entry<i1, dense<8> : vector<2xi32>>, #dlti.dl_entry<i8, dense<8> : vector<2xi32>>, #dlti.dl_entry<i16, dense<16> : vector<2xi32>>, #dlti.dl_entry<i32, dense<32> : vector<2xi32>>, #dlti.dl_entry<f16, dense<16> : vector<2xi32>>, #dlti.dl_entry<f64, dense<64> : vector<2xi32>>, #dlti.dl_entry<f128, dense<128> : vector<2xi32>>>, llvm.data_layout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128", llvm.target_triple = "x86_64-unknown-linux-gnu", "polygeist.target-cpu" = "x86-64", "polygeist.target-features" = "+cx8,+fxsr,+mmx,+sse,+sse2,+x87", "polygeist.tune-cpu" = "generic"} : () -> ()

