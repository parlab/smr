"builtin.func"() ( {
^bb0(%arg0: !fir.ref<i32>, %arg1: !fir.ref<i32>):  // no predecessors
  %0 = "arith.constant"() {value = 1 : index} : () -> index
  %1 = "fir.alloca"() {bindc_name = "i", in_type = i32, operand_segment_sizes = dense<0> : vector<2xi32>, uniq_name = "_QFpg2Ei"} : () -> !fir.ref<i32>
  %2 = "fir.load"(%arg1) : (!fir.ref<i32>) -> i32
  %3 = "fir.convert"(%2) : (i32) -> index
  %4 = "fir.do_loop"(%0, %3, %0) ( {
  ^bb0(%arg2: index):  // no predecessors
    %6 = "fir.convert"(%arg2) : (index) -> i32
    "fir.store"(%6, %1) : (i32, !fir.ref<i32>) -> ()
    %7 = "fir.load"(%arg0) : (!fir.ref<i32>) -> i32
    %8 = "fir.load"(%arg0) : (!fir.ref<i32>) -> i32
    %9 = "arith.addi"(%7, %8) : (i32, i32) -> i32
    "fir.store"(%9, %arg0) : (i32, !fir.ref<i32>) -> ()
    %10 = "arith.addi"(%arg2, %0) : (index, index) -> index
    "fir.result"(%10) : (index) -> ()
  }) {finalValue} : (index, index, index) -> index
  %5 = "fir.convert"(%4) : (index) -> i32
  "fir.store"(%5, %1) : (i32, !fir.ref<i32>) -> ()
  "std.return"() : () -> ()
}) {arg_attrs = [{fir.bindc_name = "value"}, {fir.bindc_name = "iters"}], sym_name = "_QPpg2", type = (!fir.ref<i32>, !fir.ref<i32>) -> ()} : () -> ()
