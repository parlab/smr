// MATCH ERRORS:
//  - Extra operation to perform a type cast breaks the CDG match
//
// POSSIBLE FIX:
//  - Canonicalize unnecessary implicit casts
//  * Necessary casts have to be matched by the pattern as well (no fix)
//
// SHOULD MATCH:
//     void local_const(int **matrix, int *acc, int n, int m)
//     {
//         for (int i = 0; i < 16; i++) {
//             acc[i] += 5.0; /*5.0 is already a float*/
//         }
//     }
//
// EXPLANATION:
//  There might be unnecessary conversions in an input code. The exemple below
//  could simply be 5.0 instead of 5, removing the implicit cast from an int
//  to a float. Having this cast implies having en extra operation in CIL to
//  perform this cast. If unnecessary, it might prevent otherwise successful
//  matches.
//
void sum5(float* acc)
{
    for (int i = 0; i < 16; i++) {
        acc[i] += 5; /*Integer 5 is casted to float*/
    }
}