// MATCH ERRORS:
//  - No pattern can represent the for below due to the inner workins of arrays
//
// POSSIBLE FIX:
//  - Maybe a preprocessing pass to generate patterns that support arrays with
//    variable dimensions.
//
// SHOULD MATCH:
//     void sumvec(int acc, int vec[] /*this is a !cil.pointer<!cil.int>*/)
//     {
//         for (int i = 0; i < 5; i++)
//         {
//             acc += vec[i];
//         }
//     }
//
// EXPLANATION:
//  In C, when passing arrays as a function parameter, the first dimension is
//  never specified, simply because it is not necessary for memory access, only
//  for bound checking (which C does no support). For 2D arrays, however, we
//  need to know the length of each row for row-wise access: M[1][0] is only
//  accessible if we know how many values there are in the first row (M[0]).
//  What this implies in CIL, is that it is impossible to pass a pure 2D array
//  to a function, as the first dimensions are always converted to a pointer.
//  Hence, with our pattern representation wrapped in functions, we can only
//  represent arrays if they are locally declared within the funcion.
//
//  NOTE: I think CIL does not support variable sized arrays.
//
void sumvec()
{
    int acc = 0, size = 5;
    int vec[] = {1, 2, 3, 4, 5}; /*this is a !cil.array<5 x !cil.int>*/

    for (int i = 0; i < size; i++)
    {
        acc += vec[i];
    }
}