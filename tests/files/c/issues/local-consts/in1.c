// MATCH ERRORS:
//  - Wildcard tokens only match operations in the wrapper function context
//
// POSSIBLE FIX:
//  - Allow wildcards tokens to be inside a region of the pattern
//
// SHOULD MATCH:
//     void local_const(int **matrix, int *acc, int n, int m)
//     {
//         for (int i = 0; i < n; i++) {
//             for (int j = 0; j < m; j++) {
//                 *acc += matrix[i][j];
//             }
//         }
//     }
//
// EXPLANATION:
//  A constant declared inside a scope in C will be defined within the
//  equivalent MLIR region. This means that the '4' in nested loop below would
//  convert to a 'std.constant' inside the first loop region. If the pattern
//  is using a input variable in that position (AKA a wildcard token), it would
//  not match due to the fact that wildcard tokens are only expected outside
//  the fist RDO in the pattern.
//
void local_const(int **matrix, int *acc)
{
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4/*This const is an issue*/; j++) {
            *acc += matrix[i][j];
        }
    }
}
