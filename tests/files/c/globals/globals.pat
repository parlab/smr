c {
    int a, b;  // a and b can be any integer global value
    void test1(int *sum, int n)
    {
        for (int i=0; i < n; ++i)
            *sum += a + b;
    }
}={
    void test1(int *sum, int n)
    {
        // Test unrestricted global use
        return;
    }
}

c {
    int a;
    void test2(int *sum, int n)
    {
        for (int i=0; i < n; ++i)
            *sum += a + a;  // values in sum must be eaqual
    }
}={
    void test2(int *sum, int n)
    {
        // Test global use with implicit equality
        return;
    }
}

c {
    int a = 10, b = 5;  // global must have a specific value
    void test3(int *sum, int n)
    {
        for (int i=0; i < n; ++i)
            *sum += a + b;
    }
}={
    void test3(int *sum, int n)
    {
        // Test global use with defined values
        return;
    }
}

c {
    int a = 10, b = 10;  // global must have a specific value
    void test3(int *sum, int n)
    {
        for (int i=0; i < n; ++i)
            *sum += a + b;
    }
}={
    void test3(int *sum, int n)
    {
        // Test global use with defined values
        return;
    }
}
