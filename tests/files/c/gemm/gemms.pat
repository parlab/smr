c {
    void basicSgemm(int m, int n, int k,
                    float alpha, float beta,
                    const float *A, const float *B, float *C, 
                    int lda, int ldb, int ldc)
    {
        for (int mm = 0; mm < m; ++mm)
        {
            for (int nn = 0; nn < n; ++nn)
            {
                float c = 0.0f;
                for (int i = 0; i < k; ++i)
                {
                    float a = A[mm + i * lda];
                    float b = B[nn + i * ldb];
                    c += a * b;
                }
                C[mm + nn * ldc] = C[mm + nn * ldc] * beta + alpha * c;
            }
        }
    }
}={
    void gemm0(int m, int n, int k,
               float alpha, float beta,
               const float *A, const float *B, float *C, 
               int lda, int ldb, int ldc)
    {
        //printf("");
    }
}



c {
    void basicSgemm(int m, int n, int k,
                    float alpha, float beta,
                    const float *A, const float *B, float *C,
                    int lda, int ldb, int ldc)
    {
        for (int mm = 0; mm < m; ++mm)
        {
            for (int nn = 0; nn < n; ++nn)
            {
                float c = 0.0f;
                for (int i = 0; i < k; ++i)
                {
                    float a = A[mm + i * lda];
                    float b = B[nn + i * ldb];
                    c += alpha * a * b;
                }
                C[mm + nn * ldc] = c + beta * C[mm + nn * ldc];
            }
        }
    }
}={
    void gemm1(int m, int n, int k,
               float alpha, float beta,
               const float *A, const float *B, float *C, 
               int lda, int ldb, int ldc)
    {
        //printf("");
    }
}



c {
    void basicSgemm(int m, int n, int k,
                    float alpha,  float beta,
                    const float **A, const float **B, float **C)
    {
        for (int mm = 0; mm < m; ++mm)
        {
            for (int nn = 0; nn < n; ++nn)
            {
                float c = 0.0f;
                for (int i = 0; i < k; ++i)
                {
                    float a = A[mm][i];
                    float b = B[nn][i];
                    c += a * b;
                }
                C[mm][nn] = C[mm][nn] * beta + alpha * c;
            }
        }
    }
}={
    void gemm2(int m, int n, int k,
               float alpha,  float beta,
               const float **A, const float **B, float **C)
    {
        //printf("");
    }
}



c {
    void basicSgemm(int m, int n, int k,
                    float alpha,  float beta,
                    const float **A, const float **B, float **C)
    {
        for (int mm = 0; mm < m; ++mm)
        {
            for (int nn = 0; nn < n; ++nn)
            {
                float c = 0.0f;
                for (int i = 0; i < k; ++i)
                {
                    float a = A[mm][i];
                    float b = B[nn][i];
                    c += alpha * a * b;
                }
                C[mm][nn] = c + C[mm][nn] * beta;
            }
        }
    }
}={
    void gemm3(int m, int n, int k,
               float alpha,  float beta,
               const float **A, const float **B, float **C)
    {
        //printf("");
    }
}



c {
    void basicSgemm(int m, int n, int k,
                    float alpha,  float beta,
                    const float **A, const float **B, float **C)
    {
        for (int mm = 0; mm < m; ++mm)
        {
            for (int nn = 0; nn < n; ++nn)
            {
                float c = 0.0f;
                for (int i = 0; i < k; ++i)
                {
                    float a = A[mm][i];
                    float b = B[nn][i];
                    c += alpha * a * b;
                }
                C[mm][nn] = c;
            }
        }
    }
}={
    void gemm4(int m, int n, int k,
               float alpha,  float beta,
               const float **A, const float **B, float **C)
    {
        //printf("");
    }
}



c {
    void basicSgemm(int m, int n, int k,
                    float alpha,  float beta,
                    const float **A, const float **B, float **C)
    {
        for (int mm = 0; mm < m; ++mm)
        {
            for (int nn = 0; nn < n; ++nn)
            {
                float c = 0.0f;
                for (int i = 0; i < k; ++i)
                {
                    float a = A[mm][i];
                    float b = B[nn][i];
                    c += a * b;
                }
                C[mm][nn] = alpha * c;
            }
        }
    }
}={
    void gemm5(int m, int n, int k,
               float alpha,  float beta,
               const float **A, const float **B, float **C)
    {
        //printf("");
    }
}



c {
    void basicSgemm(int m, int n, int k,
                    float alpha,  float beta,
                    const float **A, const float **B, float **C)
    {
        for (int mm = 0; mm < m; ++mm)
        {
            for (int nn = 0; nn < n; ++nn)
            {
                for (int i = 0; i < k; ++i)
                {
                    float a = A[mm][i];
                    float b = B[nn][i];
                    C[mm][nn] += alpha * a * b;
                }
            }
        }
    }
}={
    void gemm6(int m, int n, int k,
               float alpha,  float beta,
               const float **A, const float **B, float **C)
    {
        //printf("");
    }
}



c {
    void basicSgemm(int m, int n, int k,
                    float alpha,  float beta,
                    const float **A, const float **B, float **C)
    {
        for (int mm = 0; mm < m; ++mm)
        {
            for (int nn = 0; nn < n; ++nn)
            {
                float c = 0.0f;
                for (int i = 0; i < k; ++i)
                {
                    float a = A[mm][i];
                    float b = B[nn][i];
                    c += a * b;
                }
                C[mm][nn] = beta * C[mm][nn] + c;
            }
        }
    }
}={
    void gemm7(int m, int n, int k,
               float alpha,  float beta,
               const float **A, const float **B, float **C)
    {
        //printf("");
    }
}



c {
    void basicSgemm(int m, int n, int k,
                    float alpha, float beta,
                    const float *A, const float *B, float *C, 
                    int lda, int ldb, int ldc)
    {
        for (int mm = 0; mm < m; ++mm)
        {
            for (int nn = 0; nn < n; ++nn)
            {
                float c = 0.0f;
                for (int i = 0; i < k; ++i)
                {
                    float a = A[mm + i * lda];
                    float b = B[nn + i * ldb];
                    c += a * b;
                }
                C[mm + nn * ldc] = beta * C[mm + nn * ldc] + c;
            }
        }
    }
}={
    void gemm8(int m, int n, int k,
               float alpha, float beta,
               const float *A, const float *B, float *C, 
               int lda, int ldb, int ldc)
    {
        //printf("");
    }
}



c {
    void basicSgemm(int m, int n, int k,
                    float alpha, float beta,
                    const float *A, const float *B, float *C, 
                    int lda, int ldb, int ldc)
    {
        for (int mm = 0; mm < m; ++mm)
        {
            for (int nn = 0; nn < n; ++nn)
            {
                float c = 0.0f;
                for (int i = 0; i < k; ++i)
                {
                    float a = A[mm + i * lda];
                    float b = B[nn + i * ldb];
                    c += a * b;
                }
                C[mm + nn * ldc] = alpha * c + C[mm + nn * ldc];
            }
        }
    }
}={
    void gemm9(int m, int n, int k,
               float alpha, float beta,
               const float *A, const float *B, float *C, 
               int lda, int ldb, int ldc)
    {
        //printf("");
    }
}



c {
    void basicSgemm(int m, int n, int k,
                    float alpha, float beta,
                    const float *A, const float *B, float *C, 
                    int lda, int ldb, int ldc)
    {
        for (int mm = 0; mm < m; ++mm)
        {
            for (int nn = 0; nn < n; ++nn)
            {
                float c = 0.0f;
                for (int i = 0; i < k; ++i)
                {
                    float a = A[mm + i * lda];
                    float b = B[nn + i * ldb];
                    c += alpha * a * b;
                }
                C[mm + nn * ldc] = c + C[mm + nn * ldc];
            }
        }
    }
}={
    void gemm10(int m, int n, int k,
                float alpha, float beta,
                const float *A, const float *B, float *C, 
                int lda, int ldb, int ldc)
    {
        //printf("");
    }
}



c {
    void basicSgemm(int m, int n, int k,
                    float alpha, float beta,
                    const float *A, const float *B, float *C, 
                    int lda, int ldb, int ldc)
    {
        for (int mm = 0; mm < m; ++mm)
        {
            for (int nn = 0; nn < n; ++nn)
            {
                for (int i = 0; i < k; ++i)
                {
                    float a = A[mm + i * lda];
                    float b = B[nn + i * ldb];
                    C[mm + nn * ldc] += alpha * a * b;
                }
            }
        }
    }
}={
    void gemm11(int m, int n, int k,
                float alpha, float beta,
                const float *A, const float *B, float *C, 
                int lda, int ldb, int ldc)
    {
        //printf("");
    }
}



c {
    void basicSgemm(int m, int n, int k,
                    float alpha, float beta,
                    const float *A, const float *B, float *C, 
                    int lda, int ldb, int ldc)
    {
        for (int mm = 0; mm < m; ++mm)
        {
            for (int nn = 0; nn < n; ++nn)
            {
                float c = 0.0f;
                for (int i = 0; i < k; ++i)
                {
                    float a = A[mm + i * lda];
                    float b = B[nn + i * ldb];
                    c += a * b;
                }
                C[mm + nn * ldc] = c;
            }
        }
    }
}={
    void gemm12(int m, int n, int k,
                float alpha, float beta,
                const float *A, const float *B, float *C, 
                int lda, int ldb, int ldc)
    {
        //printf("");
    }
}



c {
    void basicSgemm(int m, int n, int k,
                    float alpha, float beta,
                    const float *A, const float *B, float *C, 
                    int lda, int ldb, int ldc)
    {
        for (int mm = 0; mm < m; ++mm)
        {
            for (int nn = 0; nn < n; ++nn)
            {
                C[mm + nn * ldc] = 0.0f;
                for (int i = 0; i < k; ++i)
                {
                    float a = A[mm + i * lda];
                    float b = B[nn + i * ldb];
                    C[mm + nn * ldc] += a * b;
                }
            }
        }
    }
}={
    void gemm13(int m, int n, int k,
                float alpha, float beta,
                const float *A, const float *B, float *C, 
                int lda, int ldb, int ldc)
    {
        //printf("");
    }
}



c {
    void basicSgemm(int m, int n, int k,
                    float alpha,  float beta,
                    const float **A, const float **B, float **C)
    {
        for (int mm = 0; mm < m; ++mm)
        {
            for (int nn = 0; nn < n; ++nn)
            {
                float c = 0.0f;
                for (int i = 0; i < k; ++i)
                {
                    float a = A[mm][i];
                    float b = B[nn][i];
                    c += a * b;
                }
                C[mm][nn] = c;
            }
        }
    }
}={
    void gemm14(int m, int n, int k,
                float alpha,  float beta,
                const float **A, const float **B, float **C)
    {
        //printf("");
    }
}



c {
    void basicSgemm(int m, int n, int k,
                    float alpha,  float beta,
                    const float **A, const float **B, float **C)
    {
        for (int mm = 0; mm < m; ++mm)
        {
            for (int nn = 0; nn < n; ++nn)
            {
                C[mm][nn] = 0.0f;
                for (int i = 0; i < k; ++i)
                {
                    float a = A[mm][i];
                    float b = B[nn][i];
                    C[mm][nn] += a * b;
                }
            }
        }
    }
}={
    void gemm15(int m, int n, int k,
                float alpha,  float beta,
                const float **A, const float **B, float **C)
    {
        //printf("");
    }
}
