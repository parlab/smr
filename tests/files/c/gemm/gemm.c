void gemms(void)
{
    int m = 100, n = 100, k = 100;
    float alpha = 1.0, beta = 0.0;
    int lda = 100, ldb = 100, ldc = 100;

    float *A = malloc(sizeof(float) * 100 * 100);
    float *B = malloc(sizeof(float) * 100 * 100);
    float *C = malloc(sizeof(float) * 100 * 100);

    float **A2 = malloc(sizeof(float*) * 100);
    float **B2 = malloc(sizeof(float*) * 100);
    float **C2 = malloc(sizeof(float*) * 100);
    for (int i = 0; i < 100; ++i)
    {
        A2[i] = malloc(sizeof(float) * 100);
        B2[i] = malloc(sizeof(float) * 100);
        C2[i] = malloc(sizeof(float) * 100);
    }

    // GEMM 4 //
    for (int mm = 0; mm < m; ++mm) {
        for (int nn = 0; nn < n; ++nn) {
            float c = 0.0f;
            for (int i = 0; i < k; ++i) {
                float a = A2[mm][i];
                float b = B2[nn][i];
                c += alpha * a * b;
            }
            C2[mm][nn] = c;
        }
    }

    // GEMM 3 //
    for (int mm = 0; mm < m; ++mm) {
        for (int nn = 0; nn < n; ++nn) {
            float c = 0.0f;
            for (int i = 0; i < k; ++i) {
                float a = A2[mm][i];
                float b = B2[nn][i];
                c += alpha * a * b;
            }
            C2[mm][nn] = c + C2[mm][nn] * beta;
        }
    }

    // GEMM 4 //
    for (int mm = 0; mm < m; ++mm) {
        for (int nn = 0; nn < n; ++nn) {
            float c = 0.0f;
            for (int i = 0; i < k; ++i) {
                float a = A2[mm][i];
                float b = B2[nn][i];
                c += alpha * a * b;
            }
            C2[mm][nn] = c;
        }
    }

    // GEMM 2 //
    for (int mm = 0; mm < m; ++mm) {
        for (int nn = 0; nn < n; ++nn) {
            float c = 0.0f;
            for (int i = 0; i < k; ++i) {
                float a = A2[mm][i];
                float b = B2[nn][i];
                c += a * b;
            }
            C2[mm][nn] = C2[mm][nn] * beta + alpha * c;
        }
    }

    // GEMM 4 //
    for (int mm = 0; mm < m; ++mm) {
        for (int nn = 0; nn < n; ++nn) {
            float c = 0.0f;
            for (int i = 0; i < k; ++i) {
                float a = A2[mm][i];
                float b = B2[nn][i];
                c += alpha * a * b;
            }
            C2[mm][nn] = c;
        }
    }

    // GEMM 3 //
    for (int mm = 0; mm < m; ++mm) {
        for (int nn = 0; nn < n; ++nn) {
            float c = 0.0f;
            for (int i = 0; i < k; ++i) {
                float a = A2[mm][i];
                float b = B2[nn][i];
                c += alpha * a * b;
            }
            C2[mm][nn] = c + C2[mm][nn] * beta;
        }
    }

    // GEMM 1 //
    for (int mm = 0; mm < m; ++mm) {
        for (int nn = 0; nn < n; ++nn) {
            float c = 0.0f;
            for (int i = 0; i < k; ++i) {
                float a = A[mm + i * lda];
                float b = B[nn + i * ldb];
                c += alpha * a * b;
            }
            C[mm+nn*ldc] = c + beta * C[mm+nn*ldc];
        }
    }

    // GEMM 2 //
    for (int mm = 0; mm < m; ++mm) {
        for (int nn = 0; nn < n; ++nn) {
            float c = 0.0f;
            for (int i = 0; i < k; ++i) {
                float a = A2[mm][i];
                float b = B2[nn][i];
                c += a * b;
            }
            C2[mm][nn] = C2[mm][nn] * beta + alpha * c;
        }
    }

    // GEMM 3 //
    for (int mm = 0; mm < m; ++mm) {
        for (int nn = 0; nn < n; ++nn) {
            float c = 0.0f;
            for (int i = 0; i < k; ++i) {
                float a = A2[mm][i];
                float b = B2[nn][i];
                c += alpha * a * b;
            }
            C2[mm][nn] = c + C2[mm][nn] * beta;
        }
    }

    // GEMM 4 //
    for (int mm = 0; mm < m; ++mm) {
        for (int nn = 0; nn < n; ++nn) {
            float c = 0.0f;
            for (int i = 0; i < k; ++i) {
                float a = A2[mm][i];
                float b = B2[nn][i];
                c += alpha * a * b;
            }
            C2[mm][nn] = c;
        }
    }
}