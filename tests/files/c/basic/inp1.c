int func(int* vec, int n, int* result) {

    for (int i = 0; i < n; ++i)
        vec[i] = i;

    for (int i = 0; i < n; ++i)
        *result += vec[i];

    return 0;
}
