module attributes {llvm.data_layout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128", llvm.target_triple = "x86_64-unknown-linux-gnu"}  {
  func @func(%arg0: memref<?xi32>, %arg1: i32, %arg2: memref<?xi32>) -> i32 attributes {llvm.linkage = #llvm.linkage<external>} {
    %c0_i32 = arith.constant 0 : i32
    %c1 = arith.constant 1 : index
    %c0 = arith.constant 0 : index
    %0 = arith.index_cast %arg1 : i32 to index
    scf.for %arg3 = %c0 to %0 step %c1 {
      %1 = arith.index_cast %arg3 : index to i32
      memref.store %1, %arg0[%arg3] : memref<?xi32>
    }
    scf.for %arg3 = %c0 to %0 step %c1 {
      %1 = memref.load %arg0[%arg3] : memref<?xi32>
      %2 = affine.load %arg2[0] : memref<?xi32>
      %3 = arith.addi %2, %1 : i32
      affine.store %3, %arg2[0] : memref<?xi32>
    }
    return %c0_i32 : i32
  }
}
