c {
    void sum(int vec[], int n, int *result) {
        for (int i = 0; i < n; ++i)
            *result += vec[i];
    }
} = {
    void sum(int vec[], int n, int *result) {
        for (int i = 0; i < n; ++i)
            *result += vec[i];
    }
}


c {
    void sumif(int cond, int a, int b, int *c) {
        if (cond != 0)
            *c = a + b;
    }
} = {
    void sum(int a, int b) {
        if (a > b)
            b = a + a;
        else
            b = b + b;
    }
}
