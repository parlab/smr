#include "doctest.h"

#include "DDG/Globals.hpp"
#include "Data.hpp"
#include "config.h"
#include "mlir/Dialect/Arithmetic/IR/Arithmetic.h"
#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/Parser/Parser.h"
#include <fstream>
#include <iostream>

TEST_CASE("DDG Regions Manager Tests") {
  Data data;
  std::string source1, source2;
  std::stringstream stream1(source1), stream2(source2);
  std::ifstream file1(std::string(FILES_DIR) + "mlir/pg2-poly.mlir");
  std::ifstream file2(std::string(FILES_DIR) + "mlir/hello-poly.mlir");
  stream1 << file1.rdbuf();
  stream2 << file2.rdbuf();
  data.getContext()->allowUnregisteredDialects();
  data.getContext()->getOrLoadDialect<mlir::func::FuncDialect>();
  data.getContext()->getOrLoadDialect<mlir::arith::ArithmeticDialect>();
  data.addPattern(mlir::parseSourceString<mlir::ModuleOp>(stream1.str(),
                                                          data.getContext()));
  data.addPattern(mlir::parseSourceString<mlir::ModuleOp>(stream2.str(),
                                                          data.getContext()));

  SUBCASE("Should initialize globals from module op") {
    ddg::Globals globals;
    globals.initialize(data.getPattern(1));
    CHECK(globals.size() > 0);
  }

  SUBCASE("Should initialize globals from generic op") {
    ddg::Globals globals;
    globals.initialize(data.getPatternRoot(1));
    CHECK(globals.size() > 0);
  }

  SUBCASE("Should erase existing globals on initialization") {
    ddg::Globals globals;
    globals.initialize(data.getPatternRoot(1));
    CHECK(globals.size() > 1);
    globals.initialize(data.getPatternRoot(0));
    CHECK(globals.size() == 1);
  }

  SUBCASE("Should check if operation uses a global value") {
    ddg::Globals globals;
    auto root = data.getPatternRoot(1);
    root->walk([&](mlir::Operation *op) {
      if (op->getAttr("symbol") != nullptr)
        CHECK(globals.usesGlobal(op));
      else
        CHECK(!globals.usesGlobal(op));
    });
  }
}