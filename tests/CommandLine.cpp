#include "doctest.h"

#include "llvm/Support/CommandLine.h"

#include "CommandLine.hpp"
#include "Logger/Messages.hpp"

TEST_CASE("Command line validation tests") {
  SUBCASE("Fail on multiple PAT files") {
    const char *argv[] = {"smr", "file1.pat", "file2.pat"};
    int argc = sizeof(argv) / sizeof(argv[0]);
    llvm::cl::ParseCommandLineOptions(argc, argv);
    CHECK(cl::isValid() == Msg::FAIL_SINGLE_PAT);
  }
}
