#include "doctest.h"

#include "CDG/CDG.hpp"
#include "Data.hpp"
#include "Types.hpp"
#include "mlir/Parser/Parser.h"
#include <fstream>
#include <iostream>

TEST_CASE("Control Dependency Graph tests") {
  // SUBCASE("Should build a control dependency graph") {
  //   Data data;
  //   cdg::CDG cdg;
  //   std::string filepath = "tests/files/mlir/pg2-poly.mlir";
  //   std::string expected = "fir.do_loop,|,index,index,index,|,index,|,"
  //                          "index\"finalValue\",unit,{,index,8,},|,";
  //   std::ifstream file(filepath);
  //   std::string source;
  //   std::stringstream fileStream(source);

  //   data.Context.allowUnregisteredDialects();

  //   // load expected result
  //   data.addPattern(mlir::OwningOpRef<mlir::ModuleOp>());
  //   fileStream << file.rdbuf();
  //   data.Patterns[0] = std::move(mlir::parseSourceString<mlir::ModuleOp>(
  //       fileStream.str(), &data.Context));
  //   fileStream.clear();

  //   build control dependency graph
  //   cdg.build(data);

  //   CHECK(cdg.dump() == expected + "\n");
  // }
}
