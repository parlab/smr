#include "doctest.h"

#include "DDG/Regions.hpp"
#include "Data.hpp"
#include "config.h"
#include "mlir/Dialect/Arithmetic/IR/Arithmetic.h"
#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/Parser/Parser.h"
#include <fstream>
#include <iostream>

TEST_CASE("DDG Regions Manager Tests") {
  std::string source1, source2;
  std::stringstream stream1(source1), stream2(source2);
  std::ifstream file1(std::string(FILES_DIR) + "mlir/pg2-poly.mlir");
  std::ifstream file2(std::string(FILES_DIR) + "mlir/hello-poly.mlir");
  stream1 << file1.rdbuf();
  stream2 << file2.rdbuf();

  SUBCASE("Should reset regions") {
    Data data;
    ddg::Regions regions;

    data.getContext()->allowUnregisteredDialects();
    data.getContext()->getOrLoadDialect<mlir::func::FuncDialect>();
    data.getContext()->getOrLoadDialect<mlir::arith::ArithmeticDialect>();

    // setup sample data
    data.addPattern(mlir::parseSourceString<mlir::ModuleOp>(stream1.str(),
                                                            data.getContext()));
    data.addPattern(mlir::parseSourceString<mlir::ModuleOp>(stream2.str(),
                                                            data.getContext()));

    auto root1 = data.getPatternRoot(0);
    regions.initialize(root1);
    auto id1 = regions.get(&root1->getRegion(0));

    auto root2 = data.getPatternRoot(1);
    regions.initialize(root2);
    auto id2 = regions.get(&root2->getRegion(0));

    CHECK(id1 == id2);
  }

  SUBCASE("Should set parent regions with ID 0") {
    Data data;
    ddg::Regions regions;

    data.getContext()->allowUnregisteredDialects();
    data.getContext()->getOrLoadDialect<mlir::func::FuncDialect>();
    data.getContext()->getOrLoadDialect<mlir::arith::ArithmeticDialect>();

    // setup sample data
    data.addPattern(mlir::parseSourceString<mlir::ModuleOp>(stream1.str(),
                                                            data.getContext()));
    auto root = data.getPatternRoot(0);

    regions.initialize(root);
    CHECK(regions.get(root->getParentRegion()) == 0);
  }

  SUBCASE("Should get a region's ID") {
    Data data;
    ddg::Regions regions;

    data.getContext()->allowUnregisteredDialects();
    data.getContext()->getOrLoadDialect<mlir::func::FuncDialect>();
    data.getContext()->getOrLoadDialect<mlir::arith::ArithmeticDialect>();

    // setup sample data
    data.addPattern(mlir::parseSourceString<mlir::ModuleOp>(stream1.str(),
                                                            data.getContext()));
    auto root = data.getPatternRoot(0);
    auto region = &root->getRegion(0);

    regions.initialize(root);
    CHECK(regions.get(region) == 1);
  }
}