var searchData=
[
  ['replacementpreprocessing_278',['replacementPreprocessing',['../classfrontend_1_1F18Frontend.html#a1990a1b0bf5bf3a5605895450ed9c5f4',1,'frontend::F18Frontend::replacementPreprocessing()'],['../classfrontend_1_1FrontendInterface.html#a79926a65e371494caef6337a2e5815f1',1,'frontend::FrontendInterface::replacementPreprocessing()'],['../classfrontend_1_1PolygeistFrontend.html#a39ad8f615cc73201cd32f8ece8484cb0',1,'frontend::PolygeistFrontend::replacementPreprocessing()']]],
  ['reserverewrites_279',['reserveRewrites',['../classData.html#af3d8af1a508bd05fd3908b0ef5e85127',1,'Data']]],
  ['rewrite_280',['Rewrite',['../classRewrite.html#a20c07cee33d5e5a9e73f1af30a205786',1,'Rewrite']]],
  ['rewrite_281',['rewrite',['../classRewriter.html#a774e315cba8e4e5488b7b338a56bf0f3',1,'Rewriter']]],
  ['rewriteast_282',['RewriteAST',['../classpat_1_1RewriteAST.html#a65bd1f8f182b5d400817a7e01a9d5dff',1,'pat::RewriteAST']]],
  ['rewriter_283',['Rewriter',['../classRewriter.html#a2dca64c1c31a06c35a320ec9a4215eac',1,'Rewriter']]],
  ['run_284',['run',['../classcdg_1_1CDG.html#a6cc94742a0e55ee942d2b18480d719cc',1,'cdg::CDG::run()'],['../classddg_1_1DDG.html#a3722177ce2f39c34c6678b87063b532e',1,'ddg::DDG::run()']]]
];
