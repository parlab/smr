
#include "CDG/CDG.hpp"
#include "CommandLine.hpp"
#include "DDG/DDG.hpp"
#include "Data.hpp"
#include "Files.hpp"
#include "Frontend/Manager.hpp"
#include "Logger/Logger.hpp"
#include "Logger/Messages.hpp"
#include "Rewriter/Rewriter.hpp"
#include "mlir/Dialect/Arithmetic/IR/Arithmetic.h"
#include "mlir/Dialect/Func/IR/FuncOps.h"
#include "mlir/IR/MLIRContext.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/InitLLVM.h"
#include <cstdlib>

#define OVERVIEW "Source-based Matching and Rewriting tool.\n"

int main(int argc, char **argv) {
  Data Data;
  cdg::CDG Cdg;
  ddg::DDG Ddg;
  Rewriter Rewriter(Data.getContext());
  llvm::InitLLVM Init(argc, argv);
  llvm::cl::SetVersionPrinter(cl::versionPrinter);
  llvm::cl::ParseCommandLineOptions(argc, argv, OVERVIEW);

  // Configure mlir context.
  Data.getContext()->allowUnregisteredDialects();
  Data.getContext()->getOrLoadDialect<mlir::func::FuncDialect>();
  Data.getContext()->getOrLoadDialect<mlir::arith::ArithmeticDialect>();

  // Validate command line arguments.
  if (cl::isValid() != 0)
    return EXIT_FAILURE;

  if (!cl::Serialize.empty() && !cl::Compile)
    WARN(Msg::SERIALIZATION_ENABLED, NULL);

  // Load all input files.
  loadAllFiles(Data, Cdg, Ddg);

  // Compile flag is set: terminate after compiling.
  if (cl::Compile)
    return EXIT_SUCCESS;

  // Check dump flags.
  if (cl::DumpInputsCode)
    Data.dumpInputsCode();
  if (cl::DumpRewritesCode)
    Data.dumpRewritesCode();

  Cdg.build(Data.getPatternRoots());
  if (cl::DumpPatternsCdg)
    llvm::outs() << Cdg.dump();

  // Serialization disabled: run CDG matching.
  if (cl::Serialize.empty()) {
    INFO(Msg::START_CDG_MATCHING, NULL);
    Data.setCdgMatches(Cdg.run(Data.getInputs()));
    Data.dumpCdgMatches();
  }

  Ddg.build(Data.getPatternRoots());
  if (cl::DumpPatternsDdg)
    Ddg.dump();

  // Serialization disabled: run DDG matching.
  if (cl::Serialize.empty()) {
    INFO(Msg::START_DDG_MATCHING, NULL);
    Data.setDdgMatches(Ddg.run(Data.getCdgCandidates()));
    Data.dumpDdgMatches();
  }

  // Serialization enabled: serialize and terminate.
  if (!cl::Serialize.empty()) {
    INFO(Msg::SERIALIZING, cl::Serialize.c_str());
    std::string Filepath = cl::Serialize.c_str();
    saveObjectPatFile(Filepath, Data, Cdg, Ddg);
    return EXIT_SUCCESS;
  }

  INFO(Msg::START_REWRITING, NULL);

  auto OptimizedInputs = Rewriter.rewrite(Data.getRewrites());
  llvm::StringRef Output(cl::Output);

  // No output specified: terminate.
  if (Output.empty()) {
    WARN(Msg::NO_OUTPUT, NULL);
    return EXIT_SUCCESS;
  }

  // Output specified: write optimized inputs to file.
  writeOutputs(Data, Output, OptimizedInputs);

  return EXIT_SUCCESS;
}
