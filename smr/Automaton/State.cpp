#include "State.hpp"
#include "llvm/ADT/MapVector.h"
#include <memory>

void State::addTransition(int Symbol, int NextState) {
  if (Transitions.find(Symbol) == Transitions.end())
    Transitions[Symbol] = std::make_unique<State>(NextState);
}

State *State::next(int Token) {
  if (Transitions.find(Token) != Transitions.end())
    return Transitions[Token].get();
  return nullptr;
}
