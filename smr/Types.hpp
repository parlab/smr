#pragma once

#include <vector>

namespace smr {

template <typename T> using JagArr = std::vector<std::vector<T>>;

}
