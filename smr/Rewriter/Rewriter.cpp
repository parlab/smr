#include "Rewriter.hpp"
#include "DDG/Match.hpp"
#include "Logger/Logger.hpp"
#include "Logger/Messages.hpp"
#include "mlir/IR/BuiltinAttributes.h"
#include "mlir/IR/BuiltinOps.h"
#include "mlir/IR/Operation.h"
#include "llvm/ADT/SetVector.h"
#include <algorithm>

int Rewriter::injectDefinition(mlir::ModuleOp Input,
                               mlir::ModuleOp Definition) {
  /**
   *  FIXME: this should be reviewed later. Some details, such as updating
   *         simbols in order to allow globals from different rewrites
   *         with the same symbol to be injected in the input must be
   *         addressed.
   **/
  for (auto &Op : Definition.getBody()->getOperations()) {
    // Retrieve the operation symbol reference.
    auto Symbol = Op.getAttr("sym_name");
    auto SymbolName = getAttrValueAsString(&Symbol);

    // Symbol is not yet defined: inject definition.
    if (Input.lookupSymbol(SymbolName) == nullptr) {
      INFO(Msg::INJECTING_DEFINITION, SymbolName);
      Input.push_back(OpBuilder.clone(Op));
    }
    // Symbol already defined: notify and skip.
    else
      WARN(Msg::SYMBOL_REDEFNITION, SymbolName);
  }

  return 0;
};

void Rewriter::fillKillList(
    mlir::Operation *Op,
    llvm::SmallSetVector<mlir::Operation *, OPS> &KillList) {
  KillList.insert(Op);
  for (auto *User : Op->getUsers())
    fillKillList(User, KillList);
};

mlir::Operation *Rewriter::buildCallOp(Rewrite &Rewrite) {
  llvm::ArrayRef<mlir::Type> Results;
  llvm::SmallVector<mlir::NamedAttribute, 1> Attributes;

  // Fetch which input values matched the patterns' wrapper function arguments.
  auto Operands = Rewrite.getInputOperands();

  // Point call attribute to replacement funcition.
  auto SymNameAttr = Rewrite.getReplacementFunc()->getAttr("sym_name");
  auto Callee = getAttrValueAsString(&SymNameAttr);
  auto SymRefAttr = mlir::SymbolRefAttr::get(Rewrite.getTarget()->getContext(),
                                             Callee, llvm::None);
  Attributes.push_back(OpBuilder.getNamedAttr("callee", SymRefAttr));

  // Create and return call operation.
  auto OpState = mlir::OperationState(Rewrite.getTarget()->getLoc(), "fir.call",
                                      Operands, Results, Attributes);
  return mlir::Operation::create(OpState);
};

int Rewriter::replace(Rewrite &Rewrite) {
  llvm::SmallSetVector<mlir::Operation *, OPS> KillList;

  // Inject replacement code definitions into input code.
  injectDefinition(Rewrite.getInput(), Rewrite.getReplacement());

  // Insert call operation to the injected definition.
  auto Ip = OpBuilder.saveInsertionPoint();
  OpBuilder.setInsertionPoint(Rewrite.getTarget());
  OpBuilder.insert(buildCallOp(Rewrite));
  OpBuilder.restoreInsertionPoint(Ip);

  // Delete chain of users of the operation to be removed.
  fillKillList(Rewrite.getTarget(), KillList);
  while (!KillList.empty())
    KillList.pop_back_val()->erase();

  return 0;
};

std::set<int> Rewriter::rewrite(std::vector<Rewrite> &Rewrites) {
  std::set<int> Rewritten;
  int NextRewrite = -1;

  // Build conflicting rewrites priority.
  ConflictManager.build(Rewrites);

  // Apply rewrites in order of priority.
  while ((NextRewrite = ConflictManager.next()) >= 0) {
    INFO(Msg::APPLYING_REWRITE, NextRewrite);
    replace(Rewrites[NextRewrite]);
    Rewritten.insert(Rewrites[NextRewrite].getInputId());
  }

  return Rewritten;
};
