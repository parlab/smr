#pragma once

#include "Messages.hpp"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/FormatVariadic.h"

#include <string>

#define LOGPREFIX 5

#define INFO(code, args...) log(LogLevel::INFO, code, llvm::outs(), args)

#define WARN(code, args...) log(LogLevel::WARNING, code, llvm::outs(), args)

#define ERROR(code, args...) log(LogLevel::ERROR, code, llvm::errs(), args)

#define FATAL(code, args...) log(LogLevel::WARNING, code, llvm::errs(), args)

enum LogLevel {
  FATAL,
  ERROR,
  WARNING,
  INFO,
  DEBUG,
};

const std::array<const char *, LOGPREFIX> LogPrefix = {
    "[FATAL] ", "[ERROR] ", "[WARN] ", "[INFO] ", "[DEBUG] ",
};

/// Current verbosity level
// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
extern char LogLevel;

template <typename... VarArgs>
static int log(enum LogLevel Level, Msg Code, llvm::raw_fd_ostream &Out,
               VarArgs... Args) {
  if (LogLevel >= Level) {
    Out << LogPrefix.at(Level)
        << llvm::formatv(Messages.at(Code).Description,
                         std::forward<VarArgs>(Args)...)
        << "\n";
  }
  return Code;
}
