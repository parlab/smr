#include <string>

#include "Logger.hpp"
#include "Messages.hpp"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/FormatVariadic.h"

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
char LogLevel = LogLevel::INFO;
